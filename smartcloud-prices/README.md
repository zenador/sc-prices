# Assumptions

- That the unexpected errors by the smartcloud service returns 500 Internal Server Error (like the docker image, but usage quota exceeded error might not use this status) or any other retriable status according to HTTP spec (if it returns another status we can use `isErrorOrStatus` as the retry filter instead of the default), is transient and won't return an error many times consecutively, and will return the error fairly quickly so the retries won't take too long
- That we are expected to return the timestamp without modification (not sure what `(omitted)` in the requirements refers to) for the instance price endpoint

# Design Decisions

- Moved around some code from the original to allow code reuse across the different endpoints
- Use the same libraries as the original, but add ember client to match ember server (since ember is supposed to be blaze's successor)
- Use the name of `SmartcloudPriceService` as specified in requirements though it results in some inconsistent naming overall
- Use 2 different ways of handling different classes of errors: 1) automatic retry invisible to the API caller if the error might be transient (e.g. the error simulated in the docker image); 2) show error to the API caller directly if error seems permanent and retry is unlikely to help (e.g. 404 not found for wrong instance kinds or authorization failure if the token the smartcloud service expects is changed)
- Did not implement tests as this is just a wrapper for another API (the tests for the API return contents would make more sense there), and with the strong typing, functional programming style and compilation requirement, most issues should be caught at compilation stage instead of showing up as runtime errors

# How to Run

(The instructions on how to run this are the same as the original, so I left the original description)

Follow the instruction at [smartcloud](https://hub.docker.com/r/smartpayco/smartcloud) to run the Docker container on your machine.

Clone or download this project onto your machine and run

```
$ sbt run
```

The API should be running on your port 8080.
