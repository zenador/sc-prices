package prices

import scala.concurrent.duration.{DurationInt}

import cats.effect._
import org.http4s.ember.client.EmberClientBuilder
import org.http4s.client.Client
import org.http4s.client.middleware.RetryPolicy

object SmartClient {

  private def selectiveRetryPolicy[F[_]]: RetryPolicy[F] = { (request, result, retries) =>
    if (RetryPolicy.defaultRetriable(request, result)) {
      RetryPolicy.exponentialBackoff(
        maxWait = 500.milliseconds,
        maxRetry = 5
      )(retries)
    } else {
      None
    }
  }

  val client: Resource[IO, Client[IO]] = EmberClientBuilder
    .default[IO]
    .withRetryPolicy(selectiveRetryPolicy)
    .build

}
