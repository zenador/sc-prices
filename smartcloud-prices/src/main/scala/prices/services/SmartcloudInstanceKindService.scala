package prices.services

import cats.implicits._
import cats.effect._
import org.http4s._
import org.http4s.circe.jsonOf
import org.http4s.client.Client
import io.circe.{Decoder}

import prices.data._
import prices.config.Config.SmartcloudConfig

object SmartcloudInstanceKindService {

  def make[F[_]: Concurrent](config: SmartcloudConfig, client: Resource[F, Client[F]]): InstanceKindService[F] = new SmartcloudInstanceKindService(config, client)

  private final class SmartcloudInstanceKindService[F[_]: Concurrent](
      config: SmartcloudConfig,
      client: Resource[F, Client[F]]
  ) extends InstanceKindService[F] with SmartcloudService[F] {

    implicit val instanceKindsDecoder: Decoder[InstanceKind] = Decoder[String].map(InstanceKind.apply)

    implicit val instanceKindsEntityDecoder: EntityDecoder[F, List[InstanceKind]] = jsonOf[F, List[InstanceKind]]

    val getAllUri = "instances"

    override def getAll(): F[List[InstanceKind]] = {
      client.use { case this_client =>
        for {
          result <- this_client.expectOr[List[InstanceKind]](createRequest(getAllUri, config))(r => Concurrent[F].raiseError(SmartcloudService.Exception.APICallFailure(r.status.toString)))
        } yield (result)
      }
    }

  }

}
