package prices.services

import prices.data._

trait InstancePriceService[F[_]] {
  def getPrice(kind: InstanceKind): F[InstancePrice]
}
