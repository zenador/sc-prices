package prices.services

import scala.util.control.NoStackTrace

import cats.implicits._
import org.http4s._
import org.http4s.headers.{Authorization, Accept}

import prices.config.Config.SmartcloudConfig

trait SmartcloudService[F[_]] {

  def createRequest(uri: String, config: SmartcloudConfig): Request[F] = {
    Request[F](
      method=Method.GET,
      uri=Uri.fromString(s"${config.baseUri}/${uri}").valueOr(throw _),
      headers=Headers(
        Authorization(Credentials.Token(AuthScheme.Bearer, config.token)),
        Accept(MediaType.application.json)
      )
    )
  }

}

object SmartcloudService {

  sealed trait Exception extends NoStackTrace
  object Exception {
    case class APICallFailure(message: String) extends Exception
    case class MissingInstanceFailure(message: String) extends Exception
  }

}
