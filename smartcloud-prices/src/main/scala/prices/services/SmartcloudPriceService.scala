package prices.services

import java.time.Instant
import scala.util.Try

import cats.implicits._
import cats.effect._
import org.http4s._
import org.http4s.circe.jsonOf
import org.http4s.client.Client
import io.circe.{Decoder, HCursor}

import prices.data._
import prices.config.Config.SmartcloudConfig

object SmartcloudPriceService {

  def make[F[_]: Concurrent](config: SmartcloudConfig, client: Resource[F, Client[F]]): InstancePriceService[F] = new SmartcloudPriceService(config, client)

  private final class SmartcloudPriceService[F[_]: Concurrent](
    config: SmartcloudConfig,
    client: Resource[F, Client[F]]
  ) extends InstancePriceService[F] with SmartcloudService[F] {

    implicit val timestampDecoder: Decoder[Instant] = Decoder.decodeString.emapTry { str =>
      Try(Instant.parse(str))
    }

    implicit val instancePriceDecoder: Decoder[InstancePrice] = new Decoder[InstancePrice] {
      final def apply(c: HCursor): Decoder.Result[InstancePrice] =
        for {
          kind <- c.downField("kind").as[String].map(InstanceKind.apply)
          price <- c.downField("price").as[BigDecimal]
          priceRounded = price.setScale(2, BigDecimal.RoundingMode.HALF_UP)
          timestamp <- c.downField("timestamp").as[Instant]
        } yield {
          InstancePrice(kind, priceRounded, timestamp)
        }
    }

    implicit val instancePriceEntityDecoder: EntityDecoder[F, InstancePrice] = jsonOf[F, InstancePrice]

    val getPriceBaseUri = "instances"

    override def getPrice(kind: InstanceKind): F[InstancePrice] = {
      val getPriceUri = s"${getPriceBaseUri}/${kind.getString}"
      client.use { case this_client =>
        for {
          result <- this_client.expectOr[InstancePrice](createRequest(getPriceUri, config))(r =>
            Concurrent[F].raiseError(r.status match {
              case Status.NotFound => SmartcloudService.Exception.MissingInstanceFailure(f"No such instance kind: ${kind.getString}")
              case _ => SmartcloudService.Exception.APICallFailure(r.status.toString)
            })
          )
        } yield (result)
      }
    }

  }

}
