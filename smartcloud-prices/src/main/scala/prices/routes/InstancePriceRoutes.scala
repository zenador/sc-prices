package prices.routes

import cats.implicits._
import cats.effect._
import org.http4s.HttpRoutes
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router

import prices.routes.protocol._
import prices.services.{InstancePriceService, SmartcloudService}

final case class InstancePriceRoutes[F[_]: Sync](instancePriceService: InstancePriceService[F]) extends Http4sDsl[F] {

  val prefix = "/prices"

  implicit val instancePriceResponseEncoder = jsonEncoderOf[F, InstancePriceResponse]
  implicit val errorResponseEncoder = jsonEncoderOf[F, ErrorResponse]

  private val get: HttpRoutes[F] = HttpRoutes.of {
    case GET -> Root :? InstanceKindQueryParam(instanceKind) =>
      instancePriceService
        .getPrice(instanceKind)
        .attempt
        .flatMap {
          case Left(e: SmartcloudService.Exception.MissingInstanceFailure) => Ok(ErrorResponse(e.message))
          case Left(e) => ErrorResponse.writeErrorResponse[F](e)
          case Right(p) => Ok(InstancePriceResponse(p))
        }
  }

  def routes: HttpRoutes[F] =
    Router(
      prefix -> get
    )

}
