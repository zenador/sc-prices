package prices.routes.protocol

import java.time.Instant

import io.circe._
import io.circe.syntax._

import prices.data._

final case class InstancePriceResponse(value: InstancePrice)

object InstancePriceResponse {

  implicit val encoder: Encoder[InstancePriceResponse] =
    Encoder.instance[InstancePriceResponse] {
      case InstancePriceResponse(k) =>
        Json.obj(
          "kind" -> k.kind.getString.asJson,
          "amount" -> k.price.asJson,
          "timestamp" -> k.timestamp.asJson
        )
    }

  implicit val timestampEncoder: Encoder[Instant] = Encoder.encodeString.contramap[Instant](_.toString)

}
