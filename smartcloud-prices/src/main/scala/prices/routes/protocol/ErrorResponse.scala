package prices.routes.protocol

import cats.effect.Sync
import io.circe._
import io.circe.syntax._
import org.http4s.circe.jsonEncoderOf
import org.http4s.dsl.Http4sDsl

import prices.services.SmartcloudService

final case class ErrorResponse(msg: String)

object ErrorResponse {

  implicit val encoder: Encoder[ErrorResponse] =
    Encoder.instance[ErrorResponse] {
      case ErrorResponse(msg) =>
        Json.obj(
          "error" -> msg.asJson
        )
    }

  def writeErrorResponse[F[_]: Sync](e: Throwable) = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    implicit val errorResponseEncoder = jsonEncoderOf[F, ErrorResponse]

    e match {
      case e: SmartcloudService.Exception.APICallFailure => Ok(ErrorResponse(e.message))
      case _: java.net.ConnectException => Ok(ErrorResponse("Could not connect to smartcloud service"))
      case e => Ok(ErrorResponse(e.toString))
    }
  }

}
