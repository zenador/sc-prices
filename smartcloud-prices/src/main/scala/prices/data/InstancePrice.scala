package prices.data

import java.time.Instant

final case class InstancePrice(kind: InstanceKind, price: BigDecimal, timestamp: Instant)
